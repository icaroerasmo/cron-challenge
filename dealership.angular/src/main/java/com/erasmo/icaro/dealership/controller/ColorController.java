package com.erasmo.icaro.dealership.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.erasmo.icaro.dealership.model.Color;
import com.erasmo.icaro.dealership.service.ColorService;

@RestController
@RequestMapping("/api/color")
public class ColorController {

	@Autowired
	private ColorService service;
	
	@GetMapping(path= {"", "/"})
	public List<Color> index(){
		return service.findAll();
	}
}
