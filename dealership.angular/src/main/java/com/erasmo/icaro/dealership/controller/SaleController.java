package com.erasmo.icaro.dealership.controller;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.erasmo.icaro.dealership.dto.WebResponse;
import com.erasmo.icaro.dealership.enums.WebResponseType;
import com.erasmo.icaro.dealership.model.Sale;
import com.erasmo.icaro.dealership.service.SaleService;

@RestController
@RequestMapping("/api/sale")
public class SaleController{

	@Autowired
	private SaleService service;
	
	@GetMapping(path= {"", "/"})
	public List<Sale> index(){
		return service.findAll();
	}
	
	@GetMapping("/{id}")
	public Sale getById(@PathVariable Long id){
		
		Sale sale = service.findById(id);
		
		if(sale == null) {
			throw new EntityNotFoundException();
		}
		
		return sale;
	}
	
	@PostMapping(path= {"", "/"})
	public WebResponse save(@RequestBody Sale sale){
		service.save(sale);
		return new WebResponse(WebResponseType.SUCCESS,
				"Venda salva com sucesso!");
	}
	
	@DeleteMapping("/{id}")
	public WebResponse delete(@PathVariable Long id){
		Sale sale = service.findById(id);
		if(sale == null) {
			throw new EntityNotFoundException();
		}
		service.delete(sale);
		return new WebResponse(WebResponseType.SUCCESS,
				"Venda deletada com sucesso!");
	}
}
