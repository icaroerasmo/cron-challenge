package com.erasmo.icaro.dealership.exceptions.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class DealershipExceptionHandler extends ResponseEntityExceptionHandler {
	
	@ExceptionHandler(Exception.class)
    public ResponseEntity<String> commonException(
    	      Exception ex, WebRequest rq) {
		ResponseEntity<String> re = new ResponseEntity<String>(
				"Ocorreu um erro ao executar a solicitação", HttpStatus.INTERNAL_SERVER_ERROR);
		return re;
    }
}