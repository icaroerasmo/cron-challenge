package com.erasmo.icaro.dealership.controller;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.erasmo.icaro.dealership.dto.WebResponse;
import com.erasmo.icaro.dealership.enums.WebResponseType;
import com.erasmo.icaro.dealership.model.Automaker;
import com.erasmo.icaro.dealership.service.AutomakerService;

@RestController
@RequestMapping("/api/automaker")
public class AutomakerController{

	@Autowired
	private AutomakerService service;
	
	@GetMapping(path= {"", "/"})
	public List<Automaker> index(){
		return service.findAll();
	}
	
	@GetMapping("/{id}")
	public Automaker getById(@PathVariable Long id){
		
		Automaker automaker = service.findById(id);
		
		if(automaker == null) {
			throw new EntityNotFoundException();
		}
		
		return automaker;
	}
	
	@PostMapping(path= {"", "/"})
	public WebResponse save(@RequestBody Automaker automaker){
		service.save(automaker);
		return new WebResponse(WebResponseType.SUCCESS,
				"Montadora salva com sucesso!");
	}
	
	@DeleteMapping("/{id}")
	public WebResponse delete(@PathVariable Long id){
		Automaker automaker = service.findById(id);
		if(automaker == null) {
			throw new EntityNotFoundException();
		}
		service.delete(automaker);
		return new WebResponse(WebResponseType.SUCCESS,
				"Montadora deletada com sucesso!");
	}
}
