package com.erasmo.icaro.dealership.controller;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.erasmo.icaro.dealership.dto.WebResponse;
import com.erasmo.icaro.dealership.enums.WebResponseType;
import com.erasmo.icaro.dealership.model.Customer;
import com.erasmo.icaro.dealership.service.CustomerService;

@RestController
@RequestMapping("/api/customer")
public class CustomerController{

	@Autowired
	private CustomerService service;
	
	@GetMapping(path= {"", "/"})
	public List<Customer> index(){
		return service.findAll();
	}
	
	@GetMapping("/{id}")
	public Customer getById(@PathVariable Long id){
		
		Customer customer = service.findById(id);
		
		if(customer == null) {
			throw new EntityNotFoundException();
		}
		
		return customer;
	}
	
	@PostMapping(path= {"", "/"})
	public WebResponse save(@RequestBody Customer customer){
		service.save(customer);
		return new WebResponse(WebResponseType.SUCCESS,
				"Cliente salvo com sucesso!");
	}
	
	@DeleteMapping("/{id}")
	public WebResponse delete(@PathVariable Long id){
		Customer customer = service.findById(id);
		if(customer == null) {
			throw new EntityNotFoundException();
		}
		service.delete(customer);
		return new WebResponse(WebResponseType.SUCCESS,
				"Cliente deletado com sucesso!");
	}
}
