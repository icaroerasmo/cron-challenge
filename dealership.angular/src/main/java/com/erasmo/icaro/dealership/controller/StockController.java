package com.erasmo.icaro.dealership.controller;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.erasmo.icaro.dealership.dto.WebResponse;
import com.erasmo.icaro.dealership.enums.WebResponseType;
import com.erasmo.icaro.dealership.model.Stock;
import com.erasmo.icaro.dealership.service.StockService;

@RestController
@RequestMapping("/api/stock")
public class StockController{

	@Autowired
	private StockService service;
	
	@GetMapping(path= {"", "/"})
	public List<Stock> index(){
		return service.findAll();
	}
	
	@GetMapping("/{id}")
	public Stock getById(@PathVariable Long id){
		
		Stock stock = service.findById(id);
		
		if(stock == null) {
			throw new EntityNotFoundException();
		}
		
		return stock;
	}
	
	@PostMapping(path= {"", "/"})
	public WebResponse save(@RequestBody Stock stock){
		service.save(stock);
		return new WebResponse(WebResponseType.SUCCESS,
				"Estoque salvo com sucesso!");
	}
	
	@DeleteMapping("/{id}")
	public WebResponse delete(@PathVariable Long id){
		Stock stock = service.findById(id);
		if(stock == null) {
			throw new EntityNotFoundException();
		}
		service.delete(stock);
		return new WebResponse(WebResponseType.SUCCESS,
				"Estoque deletado com sucesso!");
	}
}
