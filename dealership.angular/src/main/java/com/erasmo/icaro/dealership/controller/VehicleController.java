package com.erasmo.icaro.dealership.controller;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.erasmo.icaro.dealership.dto.WebResponse;
import com.erasmo.icaro.dealership.enums.WebResponseType;
import com.erasmo.icaro.dealership.model.Vehicle;
import com.erasmo.icaro.dealership.service.VehicleService;

@RestController
@RequestMapping("/api/vehicle")
public class VehicleController{

	@Autowired
	private VehicleService service;
	
	@GetMapping(path= {"", "/"})
	public List<Vehicle> index(){
		return service.findAll();
	}
	
	@GetMapping("/{id}")
	public Vehicle getById(@PathVariable Long id){
		
		Vehicle vehicle = service.findById(id);
		
		if(vehicle == null) {
			throw new EntityNotFoundException();
		}
		
		return vehicle;
	}
	
	@PostMapping(path= {"", "/"})
	public WebResponse save(@RequestBody Vehicle vehicle){
		service.save(vehicle);
		return new WebResponse(WebResponseType.SUCCESS,
				"Veículo salvo com sucesso!");
	}
	
	@DeleteMapping("/{id}")
	public WebResponse delete(@PathVariable Long id){
		Vehicle vehicle = service.findById(id);
		if(vehicle == null) {
			throw new EntityNotFoundException();
		}
		service.delete(vehicle);
		return new WebResponse(WebResponseType.SUCCESS,
				"Veículo salvo com sucesso!");
	}
}
