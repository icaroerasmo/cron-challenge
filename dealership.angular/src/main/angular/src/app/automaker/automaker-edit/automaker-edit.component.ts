import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AutomakerService } from '../../shared/services/automaker/automaker.service';
import { Automaker } from '../../shared/models/automaker';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-automaker-edit',
  templateUrl: './automaker-edit.component.html',
  styleUrls: ['./automaker-edit.component.css']
})
export class AutomakerEditComponent implements OnInit {

  automaker: Automaker;
  form: FormGroup;

  constructor(private route: ActivatedRoute,
  				private automakerService: AutomakerService,
  				private router: Router,
  				private fb: FormBuilder) {
  	this.form = fb.group({
	    "name": new FormControl("", Validators.required)
	});
  }

  ngOnInit() {
  	let id = +this.route.snapshot.paramMap.get('id');
  	if(id){
  		this.automakerService.getOne(id).subscribe(
  			a => {
  				this.automaker = a;
  				this.form.controls['name'].setValue(this.automaker.name);
  			}
  		);
  	}
  }
  
  save() {
  	let automaker: Automaker = this.form.value;
  	if(this.automaker){
  		automaker.id = this.automaker.id;
  	}
  	this.automakerService.
  	save(automaker).subscribe(
  		resp => {
  			this.router.navigate(['/automaker']);
  		}
  	);
  }
}
