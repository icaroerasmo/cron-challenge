import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutomakerEditComponent } from './automaker-edit.component';

describe('AutomakerEditComponent', () => {
  let component: AutomakerEditComponent;
  let fixture: ComponentFixture<AutomakerEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutomakerEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutomakerEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
