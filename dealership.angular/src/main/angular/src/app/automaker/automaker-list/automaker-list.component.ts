import { Component, OnInit } from '@angular/core';
import { Automaker } from '../../shared/models/automaker';
import { AutomakerService } from '../../shared/services/automaker/automaker.service';

@Component({
  selector: 'app-automaker-list',
  templateUrl: './automaker-list.component.html',
  styleUrls: ['./automaker-list.component.css']
})
export class AutomakerListComponent implements OnInit {

  automakers: Automaker[];

  constructor(private automakerService:AutomakerService) { }

  ngOnInit() {
  	this.automakerService.getAll().subscribe(
  		as => { 
  			this.automakers = as;
  		}
  	);
  }
  
  delete(automaker: Automaker){
  	this.automakerService.delete(automaker.id).subscribe(
  		resp => {
  			let index = this.automakers.indexOf(automaker);
  			if(index > -1){
  				this.automakers.splice(index, 1);
  			}
  		}
  	);
  }
}
