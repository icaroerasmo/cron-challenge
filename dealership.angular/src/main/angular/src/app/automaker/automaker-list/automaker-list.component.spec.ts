import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutomakerListComponent } from './automaker-list.component';

describe('AutomakerListComponent', () => {
  let component: AutomakerListComponent;
  let fixture: ComponentFixture<AutomakerListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutomakerListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutomakerListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
