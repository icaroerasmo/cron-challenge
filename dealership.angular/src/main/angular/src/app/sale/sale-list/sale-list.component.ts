import { Component, OnInit } from '@angular/core';
import { Sale } from '../../shared/models/sale';
import { SaleService } from '../../shared/services/sale/sale.service';

@Component({
  selector: 'app-sale-list',
  templateUrl: './sale-list.component.html',
  styleUrls: ['./sale-list.component.css']
})
export class SaleListComponent implements OnInit {

   sales: Sale[];

  constructor(private saleService:SaleService) { }

  ngOnInit() {
  	this.saleService.getAll().subscribe(
  		vs => { 
  			this.sales = vs;
  		}
  	);
  }
  
  delete(sale: Sale){
  	this.saleService.delete(sale.id).subscribe(
  		resp => {
  			let index = this.sales.indexOf(sale);
  			if(index > -1){
  				this.sales.splice(index, 1);
  			}
  		}
  	);
  }
}
