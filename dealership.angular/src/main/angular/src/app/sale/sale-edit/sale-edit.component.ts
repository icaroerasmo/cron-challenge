import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SaleService } from '../../shared/services/sale/sale.service';
import { StockService } from '../../shared/services/stock/stock.service';
import { CustomerService } from '../../shared/services/customer/customer.service';
import { Sale } from '../../shared/models/sale';
import { Stock } from '../../shared/models/stock';
import { Customer } from '../../shared/models/customer';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-sale-edit',
  templateUrl: './sale-edit.component.html',
  styleUrls: ['./sale-edit.component.css']
})
export class SaleEditComponent implements OnInit {

  sale: Sale;
  form: FormGroup;
  
  stocks: Stock[];
  customers: Customer[];

  constructor(private route: ActivatedRoute,
  				private saleService: SaleService,
  				private stockService: StockService,
  				private customerService: CustomerService,
  				private router: Router,
  				private fb: FormBuilder) {
  	this.form = fb.group({
	    "stock": new FormControl(null, Validators.required),
	    "customer": new FormControl(null, Validators.required)
	});
  }

  ngOnInit() {
  	this.loadStocks();
  	this.loadCustomers();
  	this.loadEntity();
  }
  
  loadStocks(){
  	this.stockService.getAll().subscribe(
		ss => {
			this.stocks = ss;
		}
	);
  }
  
  loadCustomers(){
  	this.customerService.getAll().subscribe(
		cs => {
			this.customers = cs;
		}
	);
  }
  
  loadEntity(){
  	let id = +this.route.snapshot.paramMap.get('id');
  	if(id){
  		this.saleService.getOne(id).subscribe(
  			v => {
  				let controls = this.form.controls;
  				this.sale = v;
  				controls['stock'].setValue(this.sale.stock);
  				controls['customer'].setValue(this.sale.customer);
  			}
  		);
  	}
  }
  
  compareFn(c1: any, c2:any): boolean {     
     return c1 && c2 ? c1.id === c2.id : c1 === c2; 
  }
  
  save() {
  	let sale: Sale = this.form.value;
  	if(this.sale){
  		sale.id = this.sale.id;
  	}
  	this.saleService.
  	save(sale).subscribe(
  		resp => {
  			this.router.navigate(['/sale']);
  		}
  	);
  }
}
