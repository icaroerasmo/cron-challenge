import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerService } from '../../shared/services/customer/customer.service';
import { Customer } from '../../shared/models/customer';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-customer-edit',
  templateUrl: './customer-edit.component.html',
  styleUrls: ['./customer-edit.component.css']
})
export class CustomerEditComponent implements OnInit {

  customer: Customer;
  form: FormGroup;
  
  personTypes: any[] = [{id: 1, type: 'Física'},
  						{id: 2, type: 'Jurídica'}];

  constructor(private route: ActivatedRoute,
  				private customerService: CustomerService,
  				private router: Router,
  				private fb: FormBuilder) {
  	this.form = fb.group({
	    "name": new FormControl("", Validators.required),
	    "personType": new FormControl(null, Validators.required)
	});				
  }

  ngOnInit() {
  	let id = +this.route.snapshot.paramMap.get('id');
  	if(id){
  		this.customerService.getOne(id).subscribe(
  			c => {
  				let controls = this.form.controls;
  			
  				this.customer = c;
  				
  				controls['name'].setValue(this.customer.name);
  				controls['personType'].setValue(this.customer.personType);
  			}
  		);
  	}
  }
  
  save() {
  	let customer: Customer = this.form.value;
  	if(this.customer){
  		customer.id = this.customer.id;
  	}
  	this.customerService.
  	save(customer).subscribe(
  		resp => {
  			this.router.navigate(['/customer']);
  		}
  	);
  }
}
