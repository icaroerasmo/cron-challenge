import { Component, OnInit } from '@angular/core';
import { Customer } from '../../shared/models/customer';
import { CustomerService } from '../../shared/services/customer/customer.service';


@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css']
})
export class CustomerListComponent implements OnInit {

  customers: Customer[];

  constructor(private customerService: CustomerService) { }

  ngOnInit() {
  	this.customerService.getAll().subscribe(
  		as => { 
  			this.customers = as;
  		}
  	);
  }
  
  delete(customer: Customer){
  	this.customerService.delete(customer.id).subscribe(
  		resp => {
  			let index = this.customers.indexOf(customer);
  			if(index > -1){
  				this.customers.splice(index, 1);
  			}
  		}
  	);
  }
}
