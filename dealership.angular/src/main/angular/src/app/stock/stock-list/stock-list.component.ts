import { Component, OnInit } from '@angular/core';
import { Stock } from '../../shared/models/stock';
import { StockService } from '../../shared/services/stock/stock.service';

@Component({
  selector: 'app-stock-list',
  templateUrl: './stock-list.component.html',
  styleUrls: ['./stock-list.component.css']
})
export class StockListComponent implements OnInit {

  stocks: Stock[];

  constructor(private stockService:StockService) { }

  ngOnInit() {
  	this.stockService.getAll().subscribe(
  		vs => { 
  			this.stocks = vs;
  		}
  	);
  }
  
  delete(stock: Stock){
  	this.stockService.delete(stock.id).subscribe(
  		resp => {
  			let index = this.stocks.indexOf(stock);
  			if(index > -1){
  				this.stocks.splice(index, 1);
  			}
  		}
  	);
  }
}
