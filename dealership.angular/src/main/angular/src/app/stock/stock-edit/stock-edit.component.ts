import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StockService } from '../../shared/services/stock/stock.service';
import { VehicleService } from '../../shared/services/vehicle/vehicle.service';
import { ColorService } from '../../shared/services/color/color.service';
import { Stock } from '../../shared/models/stock';
import { Vehicle } from '../../shared/models/vehicle';
import { Color } from '../../shared/models/color';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-stock-edit',
  templateUrl: './stock-edit.component.html',
  styleUrls: ['./stock-edit.component.css']
})
export class StockEditComponent implements OnInit {

  stock: Stock;
  form: FormGroup;
  
  vehicles: Vehicle[];
  colors: Color[];

  constructor(private route: ActivatedRoute,
  				private stockService: StockService,
  				private vehicleService: VehicleService,
  				private colorService: ColorService,
  				private router: Router,
  				private fb: FormBuilder) {
  	this.form = fb.group({
	    "vehicle": new FormControl(null, Validators.required),
	    "color": new FormControl(null, Validators.required),
	    "price": new FormControl("", Validators.required),
	    "chassis": new FormControl("", Validators.required),
	    "licensePlate": new FormControl("", Validators.required)
	});
  }

  ngOnInit() {
  	this.loadVehicles();
  	this.loadColors();
  	this.loadEntity();
  }
  
  loadVehicles(){
  	this.vehicleService.getAll().subscribe(
		vs => {
			this.vehicles = vs;
		}
	);
  }
  
  loadColors(){
  	this.colorService.getAll().subscribe(
		cs => {
			this.colors = cs;
		}
	);
  }
  
  loadEntity(){
  	let id = +this.route.snapshot.paramMap.get('id');
  	if(id){
  		this.stockService.getOne(id).subscribe(
  			v => {
  				let controls = this.form.controls;
  				this.stock = v;
  				controls['vehicle'].setValue(this.stock.vehicle);
  				controls['color'].setValue(this.stock.color);
  				controls['price'].setValue(this.stock.price);
  				controls['chassis'].setValue(this.stock.chassis);
  				controls['licensePlate'].setValue(this.stock.licensePlate);
  			}
  		);
  	}
  }
  
  compareFn(c1: any, c2:any): boolean {     
     return c1 && c2 ? c1.id === c2.id : c1 === c2; 
  }
  
  save() {
  	let stock: Stock = this.form.value;
  	if(this.stock){
  		stock.id = this.stock.id;
  	}
  	this.stockService.
  	save(stock).subscribe(
  		resp => {
  			this.router.navigate(['/stock']);
  		}
  	);
  }
}
