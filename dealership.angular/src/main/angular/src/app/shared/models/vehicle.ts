import { Automaker } from './automaker';

export class Vehicle {
	id: number;
	name: string;
	automaker: Automaker;
}