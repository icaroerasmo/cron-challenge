import { Customer } from './customer';
import { Stock } from './stock';

export class Sale {
	id: number;
	customer: Customer;
	stock: Stock[];
}