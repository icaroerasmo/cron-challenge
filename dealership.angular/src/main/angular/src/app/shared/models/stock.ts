import { Vehicle } from './vehicle';
import { Color } from './color';

export class Stock {
	id: number;
	vehicle: Vehicle;
	color: Color;
	price: number;
	chassis: string;
	licensePlate: string;
}