import { Injectable } from '@angular/core';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

declare var $: any;

@Injectable()
export class ResponseInterceptor implements HttpInterceptor {

  constructor(private router: Router){}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    /** change request before send here **/
    return next.handle(req)
      .pipe(map((event: HttpEvent<any>) => {
        if (this.isWebResponse(event)) {
          let resp = event as HttpResponse<any>;
          this.showMsg(resp.body);
        }
        return event;
      })).pipe(catchError((err: any, caught) => {
        if (err instanceof HttpErrorResponse) {
      	  
      	  let message = '';
      	  
      	  if(err.status === 0){
      	  	message = 'Não foi possível contactar o servidor.' +
      	  			  ' Verifique a sua conexão com a internet.';
      	  } else if(err.status === 500){ 
      	  	message = err.error;
      	  	this.redirect();
      	  } else {
      	  	message = err.message;
      	  }
      	  
      	  this.showMsg({status: 'danger', message: message});
      	  
          return throwError(err);
        }
      }));
  }
  
  redirect(){
  	
  	let uri = this.router.url;
  	
  	if(uri.indexOf("automaker") > -1) {
  		this.router.navigate(['/automaker']);
	} else if(uri.indexOf("customer") > -1) {
		this.router.navigate(['/customer']);
	} else if(uri.indexOf("sale") > -1) {
		this.router.navigate(['/sale']);
	} else if(uri.indexOf("stock") > -1) {
		this.router.navigate(['/stock']);
	} else if(uri.indexOf("vehicle") > -1) {
		this.router.navigate(['/vehicle']);
	} else {
		this.router.navigate(['/home']);
	}
  }
  
  isWebResponse(event){
  	return (event instanceof HttpResponse) && event.body
        	&& event.body.status && event.body.message
  }
  
  showMsg(response){
  	$.notify({
		// options
		message: response.message 
	},{
		// settings
		type: response.status,
		delay: 3000,
		allow_dismiss: true,
		animate: {
			enter: 'animated fadeInDown',
			exit: 'animated fadeOutUp'
		}
	});
  }
}