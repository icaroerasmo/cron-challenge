import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Customer } from '../../models/customer';
import { Constants } from '../../other/constants'

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  backendPath: string;
  service: string;
	
  constructor(private http: HttpClient,
  				private constants: Constants) { 
  	this.backendPath = constants.backendPath;
  	this.service = 'customer/';				
  }
  
  getAll(): Observable<any> {
    return this.http.get(this.backendPath+this.service);
  }
  
  getOne(id: number): Observable<any> {
    return this.http.get(this.backendPath+this.service+id);
  }
  
  save(obj: Customer): Observable<any> {
  	return this.http.post(this.backendPath+this.service, obj);
  }
  
  delete(id: number): Observable<any> {
  	return this.http.delete(this.backendPath+this.service+id);
  }
}
