import { TestBed } from '@angular/core/testing';

import { AutomakerService } from './automaker.service';

describe('AutomakerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AutomakerService = TestBed.get(AutomakerService);
    expect(service).toBeTruthy();
  });
});
