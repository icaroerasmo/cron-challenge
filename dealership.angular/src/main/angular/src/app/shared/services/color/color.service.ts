import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Constants } from '../../other/constants'

@Injectable({
  providedIn: 'root'
})
export class ColorService {

  backendPath: string;
  service: string;

  constructor(private http: HttpClient,
  				private constants: Constants) {
  	this.backendPath = constants.backendPath;
  	this.service = 'color/';
  }
  
  getAll(): Observable<any> {
    return this.http.get(this.backendPath+this.service);
  }
}
