import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { VehicleService } from '../../shared/services/vehicle/vehicle.service';
import { AutomakerService } from '../../shared/services/automaker/automaker.service';
import { Vehicle } from '../../shared/models/vehicle';
import { Automaker } from '../../shared/models/automaker';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-vehicle-edit',
  templateUrl: './vehicle-edit.component.html',
  styleUrls: ['./vehicle-edit.component.css']
})
export class VehicleEditComponent implements OnInit {

  vehicle: Vehicle;
  form: FormGroup;
  
  automakers: Automaker[];

  constructor(private route: ActivatedRoute,
  				private vehicleService: VehicleService,
  				private automakerService: AutomakerService,
  				private router: Router,
  				private fb: FormBuilder) {
  	this.form = fb.group({
	    "name": new FormControl("", Validators.required),
	    "automaker": new FormControl(null, Validators.required)
	});
  }

  ngOnInit() {
  	this.loadAutomakers();
  	this.loadEntity();
  }
  
  loadAutomakers(){
  	this.automakerService.getAll().subscribe(
		as => {
			this.automakers = as;
		}
	);
  }
  
  loadEntity(){
  	let id = +this.route.snapshot.paramMap.get('id');
  	if(id){
  		this.vehicleService.getOne(id).subscribe(
  			v => {
  				let controls = this.form.controls;
  				this.vehicle = v;
  				controls['name'].setValue(this.vehicle.name);
  				controls['automaker'].setValue(this.vehicle.automaker);
  			}
  		);
  	}
  }
  
  compareFn(c1: any, c2:any): boolean {     
     return c1 && c2 ? c1.id === c2.id : c1 === c2; 
  }
  
  save() {
  	let vehicle: Vehicle = this.form.value;
  	if(this.vehicle){
  		vehicle.id = this.vehicle.id;
  	}
  	this.vehicleService.
  	save(vehicle).subscribe(
  		resp => {
  			this.router.navigate(['/vehicle']);
  		}
  	);
  }
}
