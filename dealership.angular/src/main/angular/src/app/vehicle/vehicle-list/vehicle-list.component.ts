import { Component, OnInit } from '@angular/core';
import { Vehicle } from '../../shared/models/vehicle';
import { VehicleService } from '../../shared/services/vehicle/vehicle.service';

@Component({
  selector: 'app-vehicle-list',
  templateUrl: './vehicle-list.component.html',
  styleUrls: ['./vehicle-list.component.css']
})
export class VehicleListComponent implements OnInit {

  vehicles: Vehicle[];

  constructor(private vehicleService:VehicleService) { }

  ngOnInit() {
  	this.vehicleService.getAll().subscribe(
  		vs => { 
  			this.vehicles = vs;
  		}
  	);
  }
  
  delete(vehicle: Vehicle){
  	this.vehicleService.delete(vehicle.id).subscribe(
  		resp => {
  			let index = this.vehicles.indexOf(vehicle);
  			if(index > -1){
  				this.vehicles.splice(index, 1);
  			}
  		}
  	);
  }
}
