import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from "@angular/forms";

import { Constants } from './shared/other/constants';
import { AutomakerService } from './shared/services/automaker/automaker.service';
import { ColorService } from './shared/services/color/color.service';
import { CustomerService } from './shared/services/customer/customer.service';
import { SaleService } from './shared/services/sale/sale.service';
import { StockService } from './shared/services/stock/stock.service';
import { VehicleService } from './shared/services/vehicle/vehicle.service';

import { AutomakerListComponent } from './automaker/automaker-list/automaker-list.component';
import { CustomerListComponent } from './customer/customer-list/customer-list.component';
import { SaleListComponent } from './sale/sale-list/sale-list.component';
import { StockListComponent } from './stock/stock-list/stock-list.component';
import { VehicleListComponent } from './vehicle/vehicle-list/vehicle-list.component';

import { HomeComponent } from './home/home.component';
import { AutomakerEditComponent } from './automaker/automaker-edit/automaker-edit.component';
import { CustomerEditComponent } from './customer/customer-edit/customer-edit.component';
import { SaleEditComponent } from './sale/sale-edit/sale-edit.component';
import { StockEditComponent } from './stock/stock-edit/stock-edit.component';
import { VehicleEditComponent } from './vehicle/vehicle-edit/vehicle-edit.component';

import { ResponseInterceptor } from './shared/other/response-interceptor';

const appRoutes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  {path: 'automaker', children:[
  		{ path: '', component: AutomakerListComponent },
  		{ path: 'edit', component: AutomakerEditComponent },
  		{ path: 'edit/:id', component: AutomakerEditComponent }
  	]
  },
  {path: 'customer', children:[
  		{ path: '', component: CustomerListComponent },
  		{ path: 'edit', component: CustomerEditComponent },
  		{ path: 'edit/:id', component: CustomerEditComponent }
  	]
  },
  {path: 'sale', children:[
  		{ path: '', component: SaleListComponent },
  		{ path: 'edit', component: SaleEditComponent },
  		{ path: 'edit/:id', component: SaleEditComponent }
  	]
  },
  {path: 'stock', children:[
  		{ path: '', component: StockListComponent },
  		{ path: 'edit', component: StockEditComponent },
  		{ path: 'edit/:id', component: StockEditComponent }
  	]
  },
  {path: 'vehicle', children:[
  		{ path: '', component: VehicleListComponent },
  		{ path: 'edit', component: VehicleEditComponent },
  		{ path: 'edit/:id', component: VehicleEditComponent }
  	]
  },
];

@NgModule({
  declarations: [
    AppComponent,
    AutomakerListComponent,
    CustomerListComponent,
    SaleListComponent,
    StockListComponent,
    VehicleListComponent,
    AutomakerEditComponent,
    CustomerEditComponent,
    SaleEditComponent,
    StockEditComponent,
    VehicleEditComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    )
  ],
  providers: [
  	Constants,
  	AutomakerService,
  	ColorService,
  	CustomerService,
  	SaleService,
  	StockService,
	VehicleService,
	{ provide: HTTP_INTERCEPTORS, useClass: ResponseInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
