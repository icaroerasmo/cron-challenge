package com.erasmo.icaro.dealership.exceptions;

public class PersonTypeNotFound extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1668410958654149646L;

	public PersonTypeNotFound() {
		super("Person Type Not Found");
	}
}
