package com.erasmo.icaro.dealership.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.erasmo.icaro.dealership.model.Vehicle;

public interface VehicleRepository extends JpaRepository<Vehicle, Long> {

}
