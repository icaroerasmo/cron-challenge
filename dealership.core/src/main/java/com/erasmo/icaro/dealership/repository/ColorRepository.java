package com.erasmo.icaro.dealership.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.erasmo.icaro.dealership.model.Color;

public interface ColorRepository extends JpaRepository<Color, Long> {

}
