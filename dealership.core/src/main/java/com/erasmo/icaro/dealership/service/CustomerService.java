package com.erasmo.icaro.dealership.service;

import org.springframework.stereotype.Service;

import com.erasmo.icaro.dealership.model.Customer;

@Service
public class CustomerService extends AbstractService<Customer, Long>{
	
}
