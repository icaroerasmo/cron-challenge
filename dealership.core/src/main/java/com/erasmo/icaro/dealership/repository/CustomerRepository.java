package com.erasmo.icaro.dealership.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.erasmo.icaro.dealership.model.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

}
