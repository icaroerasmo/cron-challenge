package com.erasmo.icaro.dealership.dto;

import com.erasmo.icaro.dealership.enums.WebResponseType;

public class WebResponse {
	
	private String status;
	private String message;
	
	public WebResponse(WebResponseType status, String message) {
		this.status = status.name().toLowerCase();
		this.message = message;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
