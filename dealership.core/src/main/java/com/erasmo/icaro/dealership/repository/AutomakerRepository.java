package com.erasmo.icaro.dealership.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.erasmo.icaro.dealership.model.Automaker;

public interface AutomakerRepository extends JpaRepository<Automaker, Long> {

}
