package com.erasmo.icaro.dealership.service;

import org.springframework.stereotype.Service;

import com.erasmo.icaro.dealership.model.Vehicle;

@Service
public class VehicleService extends AbstractService<Vehicle, Long>{

}
