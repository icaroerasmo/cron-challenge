package com.erasmo.icaro.dealership.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.erasmo.icaro.dealership.model.Sale;

public interface SaleRepository extends JpaRepository<Sale, Long> {

}
