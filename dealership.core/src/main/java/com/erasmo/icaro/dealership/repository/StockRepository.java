package com.erasmo.icaro.dealership.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.erasmo.icaro.dealership.model.Stock;

public interface StockRepository extends JpaRepository<Stock, Long> {

}
