package com.erasmo.icaro.dealership.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

public abstract class AbstractService<T, L> {
	
	@Autowired
	private JpaRepository<T, L> repository;
	
	public void save(T t) {
		repository.save(t);
	}
	
	public void delete(T t) {
		repository.delete(t);
	}
	
	public T findById(L id) {
		Optional<T> obj = repository.findById(id);
		return obj.isPresent() ? obj.get() : null;
	}
	
	public List<T> findAll() {
		return repository.findAll();
	}
}
