package com.erasmo.icaro.dealership.service;

import org.springframework.stereotype.Service;

import com.erasmo.icaro.dealership.model.Color;

@Service
public class ColorService extends AbstractService<Color, Long> {

}
