package com.erasmo.icaro.dealership.enums;

public enum WebResponseType {
	SUCCESS, INFO, WARNING, DANGER;
}
