package com.erasmo.icaro.dealership.service;

import java.util.Date;

import org.springframework.stereotype.Service;

import com.erasmo.icaro.dealership.model.Sale;

@Service
public class SaleService extends AbstractService<Sale, Long>{

	@Override
	public void save(Sale sale) {
		sale.setDate(new Date());
		super.save(sale);
	}
}
