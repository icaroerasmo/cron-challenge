package com.erasmo.icaro.dealership.enums;

import com.erasmo.icaro.dealership.exceptions.PersonTypeNotFound;

public enum PersonType {
	PHYSICAL(1), LEGAL(2);
	
	private Integer id;
	
	private PersonType(Integer id) {
		this.id = id;
	}
	
	public Integer getId() {
		return this.id;
	}
	
	public static PersonType get(Integer id) throws PersonTypeNotFound {
		for(PersonType p : values()) {
			if(id.equals(p.getId())) {
				return p;
			}
		}
		
		throw new PersonTypeNotFound();
	}
}
