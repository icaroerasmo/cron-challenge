package com.erasmo.icaro.dealership.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="TB_VEHICLE")
public class Vehicle {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="NAME", nullable=false, unique=true)
	private String name;
	
	@ManyToOne
	@JoinColumn(name="AUTOMAKER_ID", nullable=false)
	private Automaker automaker;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Automaker getAutomaker() {
		return automaker;
	}

	public void setAutomaker(Automaker automaker) {
		this.automaker = automaker;
	}
}
