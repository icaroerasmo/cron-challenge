package com.erasmo.icaro.dealership.model;

import java.beans.Transient;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.erasmo.icaro.dealership.enums.PersonType;
import com.erasmo.icaro.dealership.exceptions.PersonTypeNotFound;

@Entity
@Table(name="TB_CUSTOMER")
public class Customer {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="NAME", nullable=false)
	private String name;
	
	@Column(name="PERSON_TYPE", nullable=false)
	private PersonType personType;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Transient
	public PersonType getPersonTypeEnum() {
		return personType;
	}
	
	@Transient
	public void setPersonTypeEnum(PersonType clientType) {
		this.personType = clientType;
	}
	
	public Integer getPersonType() {
		return personType != null ? personType.getId() : null;
	}

	public void setPersonType(Integer id) throws PersonTypeNotFound {
		this.personType = PersonType.get(id);
	}
}
