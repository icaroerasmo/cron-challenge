package com.erasmo.icaro.dealership.service;

import org.springframework.stereotype.Service;

import com.erasmo.icaro.dealership.model.Automaker;

@Service
public class AutomakerService extends AbstractService<Automaker, Long> {
	
}
