package com.erasmo.icaro.dealership.controller;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.erasmo.icaro.dealership.dto.WebResponse;
import com.erasmo.icaro.dealership.enums.WebResponseType;
import com.erasmo.icaro.dealership.model.Automaker;
import com.erasmo.icaro.dealership.service.AutomakerService;

@Controller
@RequestMapping("/automaker")
public class AutomakerController extends AbstractController{

	@Autowired
	private AutomakerService service;
	
	@GetMapping(path= {"", "/"})
	public ModelAndView index(){
		ModelAndView mv = getModelAndView();
		mv.addObject("automakers", service.findAll());
		return mv;
	}
	
	@GetMapping("/create")
	public ModelAndView create(){
		ModelAndView mv = getModelAndView();
		mv.addObject("automaker", new Automaker());
		return mv;
	}
	
	@GetMapping("/edit/{id}")
	public ModelAndView edit(@PathVariable Long id){
		ModelAndView mv = getModelAndView();
		Automaker a = service.findById(id);
		if(a == null) {
			throw new EntityNotFoundException();
		}
		mv.addObject("automaker", a);
		return mv;
	}
	
	@PostMapping(path= {"", "/"})
	public ModelAndView save(@ModelAttribute Automaker automaker){
		service.save(automaker);
		ModelAndView mv = getModelAndView();
		mv.addObject("automakers", service.findAll());
		mv.addObject("response", new WebResponse(WebResponseType.SUCCESS,
				"Montadora salva com sucesso!"));
		return mv;
	}
	
	@GetMapping("/delete/{id}")
	public ModelAndView delete(@PathVariable Long id){
		
		Automaker a = service.findById(id);
		
		if(a == null) {
			throw new EntityNotFoundException();
		}
		
		service.delete(a);
		ModelAndView mv = getModelAndView();
		mv.addObject("automakers", service.findAll());
		mv.addObject("response", new WebResponse(WebResponseType.SUCCESS,
				"Montadora deletada com sucesso!"));
		return mv;
	}
}
