package com.erasmo.icaro.dealership.controller;

import org.springframework.web.servlet.ModelAndView;

public abstract class AbstractController {
	protected ModelAndView getModelAndView(){
		String view = this.getClass().getSimpleName().replace("Controller", "").toLowerCase();
		String method = Thread.currentThread().getStackTrace()[2].getMethodName();
		if(method.equals("create")) {
			method = "edit";
		} else if (method.equals("save") ||
				method.equals("delete")) {
			method = "index";
		}
		ModelAndView mv = new ModelAndView("index");
		mv.addObject("view", view+"/"+method);
		return mv;
	}
}
