package com.erasmo.icaro.dealership;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

import com.erasmo.icaro.dealership.model.Color;
import com.erasmo.icaro.dealership.service.ColorService;

@SpringBootApplication
public class ThymeleafApplication extends SpringBootServletInitializer {

	@Autowired
	ColorService colorService;
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ThymeleafApplication.class);
	}
	
	public static void main(String[] args) {
		SpringApplication.run(ThymeleafApplication.class, args);
	}

	@Bean
	protected void loadColors() {
		List<Color> colors = colorService.findAll();
		if(colors != null &&
				colors.size() < 1) {
			
			Color cor = new Color();
			cor.setName("Vermelho");
			colorService.save(cor);
			
			cor = new Color();
			cor.setName("Preto");
			colorService.save(cor);
			
			cor = new Color();
			cor.setName("Branco");
			colorService.save(cor);
			
			cor = new Color();
			cor.setName("Prata");
			colorService.save(cor);
			
			cor = new Color();
			cor.setName("Grafite");
			colorService.save(cor);
		}
	}
}

