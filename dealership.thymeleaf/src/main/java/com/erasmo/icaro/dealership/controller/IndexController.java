package com.erasmo.icaro.dealership.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class IndexController {

	@RequestMapping("/")
	public ModelAndView index() {
		ModelAndView index = new ModelAndView("index");
		index.addObject("view","home/index");
		return index;
	}
}
