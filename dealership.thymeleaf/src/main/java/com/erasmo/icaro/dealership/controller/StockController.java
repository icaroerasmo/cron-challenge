package com.erasmo.icaro.dealership.controller;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.erasmo.icaro.dealership.dto.WebResponse;
import com.erasmo.icaro.dealership.enums.WebResponseType;
import com.erasmo.icaro.dealership.model.Stock;
import com.erasmo.icaro.dealership.service.ColorService;
import com.erasmo.icaro.dealership.service.StockService;
import com.erasmo.icaro.dealership.service.VehicleService;

@Controller
@RequestMapping("/stock")
public class StockController extends AbstractController {
	
	@Autowired
	private StockService service;
	
	@Autowired
	private ColorService colors;
	
	@Autowired
	private VehicleService vehicles;
	
	@GetMapping(path= {"", "/"})
	public ModelAndView index(){
		ModelAndView mv = getModelAndView();
		mv.addObject("stocks", service.findAll());
		return mv;
	}
	
	@GetMapping("/create")
	public ModelAndView create(){
		ModelAndView mv = getModelAndView();
		mv.addObject("colors", colors.findAll());
		mv.addObject("vehicles", vehicles.findAll());
		mv.addObject("stock", new Stock());
		return mv;
	}
	
	@GetMapping("/edit/{id}")
	public ModelAndView edit(@PathVariable Long id){
		ModelAndView mv = getModelAndView();
		mv.addObject("colors", colors.findAll());
		mv.addObject("vehicles", vehicles.findAll());
		Stock s = service.findById(id);
		if(s == null) {
			throw new EntityNotFoundException();
		}
		mv.addObject("stock", s);
		return mv;
	}
	
	@PostMapping(path= {"", "/"})
	public ModelAndView save(@ModelAttribute Stock stock){
		service.save(stock);
		ModelAndView mv = getModelAndView();
		mv.addObject("stocks", service.findAll());
		mv.addObject("response", new WebResponse(WebResponseType.SUCCESS,
				"Veículo adicionado ao estoque com sucesso!"));
		return mv;
	}
	
	@GetMapping("/delete/{id}")
	public ModelAndView delete(@PathVariable Long id){
		
		Stock s = service.findById(id);
		
		if(s == null) {
			throw new EntityNotFoundException();
		}
		
		service.delete(s);
		ModelAndView mv = getModelAndView();
		mv.addObject("stocks", service.findAll());
		mv.addObject("response", new WebResponse(WebResponseType.SUCCESS,
				"Item removido do estoque com sucesso!"));
		return mv;
	}
}
