package com.erasmo.icaro.dealership.controller;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.erasmo.icaro.dealership.dto.WebResponse;
import com.erasmo.icaro.dealership.enums.PersonType;
import com.erasmo.icaro.dealership.enums.WebResponseType;
import com.erasmo.icaro.dealership.model.Customer;
import com.erasmo.icaro.dealership.service.CustomerService;

@Controller
@RequestMapping("/customer")
public class CustomerController extends AbstractController {

	@Autowired
	private CustomerService service;
	
	@GetMapping(path= {"", "/"})
	public ModelAndView index(){
		ModelAndView mv = getModelAndView();
		mv.addObject("customers", service.findAll());
		return mv;
	}
	
	@GetMapping("/create")
	public ModelAndView create(){
		ModelAndView mv = getModelAndView();
		mv.addObject("personTypes", PersonType.values());
		mv.addObject("customer", new Customer());
		return mv;
	}
	
	@GetMapping("/edit/{id}")
	public ModelAndView edit(@PathVariable Long id){
		ModelAndView mv = getModelAndView();
		mv.addObject("personTypes", PersonType.values());
		Customer c = service.findById(id);
		if(c == null) {
			throw new EntityNotFoundException();
		}
		mv.addObject("customer", c);
		return mv;
	}
	
	@PostMapping(path= {"", "/"})
	public ModelAndView save(@ModelAttribute Customer customer){
		service.save(customer);
		ModelAndView mv = getModelAndView();
		mv.addObject("customers", service.findAll());
		mv.addObject("response", new WebResponse(WebResponseType.SUCCESS,
				"Cliente salvo com sucesso!"));
		return mv;
	}
	
	@GetMapping("/delete/{id}")
	public ModelAndView delete(@PathVariable Long id){
		Customer c = service.findById(id);
		
		if(c == null) {
			throw new EntityNotFoundException();
		}
		
		service.delete(c);
		
		ModelAndView mv = getModelAndView();
		mv.addObject("customers", service.findAll());
		mv.addObject("response", new WebResponse(WebResponseType.SUCCESS,
				"Cliente deletado com sucesso!"));
		return mv;
	}
}
