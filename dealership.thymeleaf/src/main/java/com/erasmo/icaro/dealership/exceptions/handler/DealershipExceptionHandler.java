package com.erasmo.icaro.dealership.exceptions.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.erasmo.icaro.dealership.dto.WebResponse;
import com.erasmo.icaro.dealership.enums.WebResponseType;
import com.erasmo.icaro.dealership.service.AutomakerService;
import com.erasmo.icaro.dealership.service.CustomerService;
import com.erasmo.icaro.dealership.service.SaleService;
import com.erasmo.icaro.dealership.service.StockService;
import com.erasmo.icaro.dealership.service.VehicleService;

@ControllerAdvice
public class DealershipExceptionHandler extends ResponseEntityExceptionHandler {

	@Autowired
	AutomakerService automakerService;
	
	@Autowired
	CustomerService customerService;
	
	@Autowired
	SaleService saleService;
	
	@Autowired
	StockService stockService;
	
	@Autowired
	VehicleService vehicleService;
	
	@ExceptionHandler(Exception.class)
    public ModelAndView commonException(
    	      Exception ex, WebRequest rq) {
		
		return treatException(rq);
    }

	private ModelAndView treatException(WebRequest rq) {
		
		String uri = ((ServletWebRequest)rq).getRequest().getRequestURL().toString();
		ModelAndView mv = new ModelAndView("index");
		
		if(uri.contains("automaker")) {
			mv.addObject("view", "automaker/index");
			mv.addObject("automakers", automakerService.findAll());
		} else if(uri.contains("customer")) {
			mv.addObject("view", "customer/index");
			mv.addObject("customers", customerService.findAll());
		} else if(uri.contains("sale")) {
			mv.addObject("view", "sale/index");
			mv.addObject("sales", saleService.findAll());
		} else if(uri.contains("stock")) {
			mv.addObject("view", "stock/index");
			mv.addObject("stocks", stockService.findAll());
		} else if(uri.contains("vehicle")) {
			mv.addObject("view", "vehicle/index");
			mv.addObject("vehicles", vehicleService.findAll());
		} else {
			mv.addObject("view", "home/index");
		}
		
		mv.addObject("response", new WebResponse(
				WebResponseType.DANGER,
				"Ocorreu um erro ao executar a solicitação"));
		return mv;
	}
}
