package com.erasmo.icaro.dealership.controller;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.erasmo.icaro.dealership.dto.WebResponse;
import com.erasmo.icaro.dealership.enums.WebResponseType;
import com.erasmo.icaro.dealership.model.Vehicle;
import com.erasmo.icaro.dealership.service.AutomakerService;
import com.erasmo.icaro.dealership.service.VehicleService;

@Controller
@RequestMapping("/vehicle")
public class VehicleController extends AbstractController{

	@Autowired
	private VehicleService service;
	
	@Autowired
	private AutomakerService automakers;
	
	@GetMapping(path= {"", "/"})
	public ModelAndView index(){
		ModelAndView mv = getModelAndView();
		mv.addObject("vehicles", service.findAll());
		return mv;
	}
	
	@GetMapping("/create")
	public ModelAndView create(){
		ModelAndView mv = getModelAndView();
		mv.addObject("vehicle", new Vehicle());
		mv.addObject("automakers", automakers.findAll());
		return mv;
	}
	
	@GetMapping("/edit/{id}")
	public ModelAndView edit(@PathVariable Long id){
		ModelAndView mv = getModelAndView();
		mv.addObject("automakers", automakers.findAll());
		Vehicle v = service.findById(id);
		if(v == null) {
			throw new EntityNotFoundException();
		}
		mv.addObject("vehicle", v);
		return mv;
	}
	
	@PostMapping(path= {"", "/"})
	public ModelAndView save(@ModelAttribute Vehicle vehicle){
		service.save(vehicle);
		ModelAndView mv = getModelAndView();
		mv.addObject("vehicles", service.findAll());
		mv.addObject("response", new WebResponse(WebResponseType.SUCCESS,
				"Veículo salvo com sucesso!"));
		return mv;
	}
	
	@GetMapping("/delete/{id}")
	public ModelAndView save(@PathVariable Long id){
		
		Vehicle v = service.findById(id);
		
		if(v == null) {
			throw new EntityNotFoundException();
		}
		
		service.delete(v);
		ModelAndView mv = getModelAndView();
		mv.addObject("vehicles", service.findAll());
		mv.addObject("response", new WebResponse(WebResponseType.SUCCESS,
				"Veículo deletado com sucesso!"));
		return mv;
	}
}
