package com.erasmo.icaro.dealership.controller;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.erasmo.icaro.dealership.dto.WebResponse;
import com.erasmo.icaro.dealership.enums.WebResponseType;
import com.erasmo.icaro.dealership.model.Sale;
import com.erasmo.icaro.dealership.service.CustomerService;
import com.erasmo.icaro.dealership.service.SaleService;
import com.erasmo.icaro.dealership.service.StockService;

@Controller
@RequestMapping("/sale")
public class SaleController extends AbstractController{

	@Autowired
	private SaleService service;
	
	@Autowired
	private CustomerService customers;
	
	@Autowired
	private StockService stocks;
	
	@GetMapping(path= {"", "/"})
	public ModelAndView index(){
		ModelAndView mv = getModelAndView();
		mv.addObject("sales", service.findAll());
		return mv;
	}
	
	@GetMapping("/create")
	public ModelAndView create(){
		ModelAndView mv = getModelAndView();
		mv.addObject("customers", customers.findAll());
		mv.addObject("stocks", stocks.findAll());
		mv.addObject("sale", new Sale());
		return mv;
	}
	
	@GetMapping("/edit/{id}")
	public ModelAndView edit(@PathVariable Long id){
		ModelAndView mv = getModelAndView();
		mv.addObject("customers", customers.findAll());
		mv.addObject("stocks", stocks.findAll());
		Sale s = service.findById(id);
		if(s == null) {
			throw new EntityNotFoundException();
		}
		mv.addObject("sale", s);
		return mv;
	}
	
	@PostMapping(path= {"", "/"})
	public ModelAndView save(@ModelAttribute Sale sale){
		service.save(sale);
		ModelAndView mv = getModelAndView();
		mv.addObject("sales", service.findAll());
		mv.addObject("response", new WebResponse(WebResponseType.SUCCESS,
				"Venda realizada com sucesso!"));
		return mv;
	}
	
	@GetMapping("/delete/{id}")
	public ModelAndView delete(@PathVariable Long id){
		
		Sale s = service.findById(id);
		
		if(s == null) {
			throw new EntityNotFoundException();
		}
		
		service.delete(s);
		ModelAndView mv = getModelAndView();
		mv.addObject("sales", service.findAll());
		mv.addObject("response", new WebResponse(WebResponseType.SUCCESS,
				"Venda deletada com sucesso!"));
		return mv;
	}
}
